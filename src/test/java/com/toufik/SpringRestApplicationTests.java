package com.toufik;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class SpringRestApplicationTests {

    @Test
    void contextLoads() {
        String str = "123";
        assertThat(str).hasSize(3);
    }

}
